package com.foodstuff_interview.recipes

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.databinding.MainActivityBinding
import com.foodstuff_interview.recipes.ui.main.recipes.RecipesFragmentDirections

class MainActivity : AppCompatActivity() {
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navigationFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navigationFragment.navController
    }

    public fun navigate(recipeItem:Recipe) {
        try {
            val action = RecipesFragmentDirections.actionRecipesFragmentToRecipesDetailFragment(recipeItem)
            navController.navigate(action)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}