package com.foodstuff_interview.recipes.ui.main.recipes

import androidx.lifecycle.*
import com.foodstuff_interview.recipes.data.Result
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.data.source.RecipesRepository
import kotlinx.coroutines.launch

class RecipesViewModel(val repository: RecipesRepository) : ViewModel() {

    private val _recipes = MutableLiveData<List<Recipe>>()
    val recipes: LiveData<List<Recipe>> = _recipes

    private val _loaded = MutableLiveData<Boolean>()
    val loaded: LiveData<Boolean> = _loaded

     var _selected = SingleLiveEvent<Recipe>()
    val selected:LiveData<Recipe> = _selected

    fun select(recipeItem: Recipe) {
        _selected.value = recipeItem
    }


    fun getRecipes() {
        viewModelScope.launch {
            _loaded.value = true
            repository.getAllRecipes().let { result ->
                if (result is Result.Success) {
                    tasksLoaded(result.data)
                } else if (result is Result.Error) {
                    nothingLoaded()
                }
            }
        }
    }

    private fun tasksLoaded(recipes: List<Recipe>) {
        _recipes.value = recipes

    }

    private fun nothingLoaded() {
       _recipes.value = emptyList()
    }

}

class RecipesViewModelFactory (
    private val recipesRepository: RecipesRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>) =
        (RecipesViewModel(recipesRepository) as T)
}