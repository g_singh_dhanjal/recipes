package com.foodstuff_interview.recipes.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.databinding.RecipesListItemBinding
import com.foodstuff_interview.recipes.ui.main.recipes.RecipesViewModel

class RecipesAdapter(
    private val context: Context,
    private val listOfRecipes: List<Recipe>,
    private val recipeViewModel: RecipesViewModel
) :
    RecyclerView.Adapter<RecipesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesViewHolder {
        val listItemBinding =
            RecipesListItemBinding.inflate(LayoutInflater.from(parent.context), parent
                , false)
        println("data observed onCreateViewHolder ${listOfRecipes.size} ")
        listItemBinding.root.setOnClickListener {
            println("Item Clicked ")
            val position = listItemBinding.root.getTag() as Int
            recipeViewModel._selected.value =listOfRecipes.get(position)
        }
        return RecipesViewHolder(listItemBinding)
    }

    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        holder.bind(listOfRecipes[position], position)
    }

    override fun getItemCount(): Int {
        return listOfRecipes.size
    }
}


class RecipesViewHolder(private val binding: RecipesListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {


    fun bind(recipes: Recipe, position: Int) {
        binding.recipe = recipes

        binding.root.tag = position
    }


}