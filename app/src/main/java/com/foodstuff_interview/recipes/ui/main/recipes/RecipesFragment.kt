package com.foodstuff_interview.recipes.ui.main.recipes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.foodstuff_interview.recipes.MainActivity
import com.foodstuff_interview.recipes.RecipesApplication
import com.foodstuff_interview.recipes.data.database.RecipesDb
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.data.source.FoodStuffRecipeRepository
import com.foodstuff_interview.recipes.data.source.local.RecipesLocalDataSource
import com.foodstuff_interview.recipes.data.source.remote.RecipesRemoteDataSource
import com.foodstuff_interview.recipes.databinding.RecipesFragmentBinding
import com.foodstuff_interview.recipes.ui.main.adapter.RecipesAdapter

class RecipesFragment : Fragment() {


    val recipeViewModel by viewModels<RecipesViewModel>(){
        RecipesViewModelFactory((activity?.applicationContext as RecipesApplication).taskRepository)
    }
    lateinit var binding: RecipesFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = RecipesFragmentBinding.inflate(layoutInflater,null,false)
        binding.recipeRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recipeRecyclerView.itemAnimator = DefaultItemAnimator()
        binding.recipeRecyclerView.addItemDecoration(
            DividerItemDecoration(
                activity?.applicationContext,
                DividerItemDecoration.VERTICAL
            )
        )

        recipeViewModel.recipes.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            setUpAdapter(it,recipeViewModel)

        })
        recipeViewModel.selected.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            (activity as MainActivity).navigate(it)

        })


        return binding.root

    }

    override fun onResume() {
        recipeViewModel.getRecipes()
        super.onResume()
    }

    private fun setUpAdapter(it: List<Recipe>, recipeViewModel : RecipesViewModel) {
        println("data observed= ${it.size} ")
        val recipesAdapter = view?.context?.let { it1 -> RecipesAdapter(it1, it,recipeViewModel) }
        binding.recipeRecyclerView.adapter = recipesAdapter
        recipesAdapter?.notifyDataSetChanged()
    }
}