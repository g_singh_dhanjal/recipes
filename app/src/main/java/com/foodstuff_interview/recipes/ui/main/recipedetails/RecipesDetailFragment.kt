package com.foodstuff_interview.recipes.ui.main.recipedetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.databinding.RecipesDetailsFragmentBinding

class RecipesDetailFragment : Fragment() {

    val args: RecipesDetailFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val recipe = args.recipe

        val bind = RecipesDetailsFragmentBinding.inflate(layoutInflater, null, false)
        bind.recipe = recipe as Recipe;
        return bind.root


    }
}