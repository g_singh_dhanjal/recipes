package com.foodstuff_interview.recipes

import android.app.Application
import com.foodstuff_interview.recipes.data.source.RecipesRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RecipesApplication : Application() {

    val taskRepository: RecipesRepository
        get() = ServiceLocator.provideTasksRepository(this)

    override fun onCreate() {
        super.onCreate()
        GlobalScope.launch {
            taskRepository.getAllRecipes()
        }
    }
}