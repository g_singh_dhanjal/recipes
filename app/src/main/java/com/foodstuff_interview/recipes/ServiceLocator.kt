package com.foodstuff_interview.recipes

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.foodstuff_interview.recipes.data.database.RecipesDb
import com.foodstuff_interview.recipes.data.source.FoodStuffRecipeRepository
import com.foodstuff_interview.recipes.data.source.RecipesDataSource
import com.foodstuff_interview.recipes.data.source.RecipesRepository
import com.foodstuff_interview.recipes.data.source.local.RecipesLocalDataSource
import com.foodstuff_interview.recipes.data.source.remote.RecipesRemoteDataSource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object ServiceLocator {

    private var database: RecipesDb? = null
    @Volatile
    var recipesRepository: RecipesRepository? = null
        @VisibleForTesting set

    fun provideTasksRepository(context: Context): RecipesRepository {
        synchronized(this) {
            return recipesRepository ?: createRecipesRepository(context)
        }
    }

    private fun createRecipesRepository(context: Context): RecipesRepository {
        val newRepo = FoodStuffRecipeRepository( createRecipeLocalDataSource(context),RecipesRemoteDataSource())
        recipesRepository = newRepo
        return newRepo
    }

    private fun createRecipeLocalDataSource(context: Context): RecipesDataSource {
        val database = database ?: createDataBase(context)
        return RecipesLocalDataSource(database.recipesDao())
    }

    private fun createDataBase(context: Context): RecipesDb {
        /*val result = Room.databaseBuilder(
            context.applicationContext,
            RecipesDb::class.java, "Recipes.db"
        ).build()*/
        val result = RecipesDb.invoke(context)
        database = result

        return result
    }
}