package com.foodstuff_interview.recipes.data.database

import androidx.room.*
import com.foodstuff_interview.recipes.data.model.Recipe


@Dao
abstract  class  RecipesDao {
   /* @Query("Select * from Recipe")
    abstract suspend fun getAllRecipes(): kotlinx.coroutines.flow.Flow<List<Recipe>>*/

    @Query("Select * from Recipe")
    abstract suspend fun getAllRecipe():List<Recipe>

    @Query("Select * from Recipe where recipeId = :id")
    abstract suspend fun getRecipeById(id: Int):Recipe

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    abstract suspend fun insert( recipe: Recipe) : Long

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll( recipes: List<Recipe>)

    @Query("DELETE FROM Recipe")
    abstract suspend fun deleteAll()


}