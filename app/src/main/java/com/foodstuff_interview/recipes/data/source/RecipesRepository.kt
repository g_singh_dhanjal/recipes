package com.foodstuff_interview.recipes.data.source

import androidx.lifecycle.LiveData
import com.foodstuff_interview.recipes.data.Result
import com.foodstuff_interview.recipes.data.model.Recipe

interface RecipesRepository {
    suspend fun getAllRecipes(): Result<List<Recipe>>

    suspend fun getRecipeById(id: Int): Result<Recipe>

    suspend fun insertAll(recipes: List<Recipe>)
}