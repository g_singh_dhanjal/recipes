package com.foodstuff_interview.recipes.data.source

import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.data.Result
interface RecipesDataSource {

    suspend fun getAllRecipes(): Result<List<Recipe>>

    suspend fun getRecipeById(id: Int): Result<Recipe>

    suspend fun insertAll(recipes: List<Recipe>)
}