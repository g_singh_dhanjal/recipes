package com.foodstuff_interview.recipes.data.database.typeconverters

import androidx.room.TypeConverter
import com.foodstuff_interview.recipes.data.model.Ingredients
import com.foodstuff_interview.recipes.data.model.Recipe
import com.google.gson.reflect.TypeToken
import java.util.*

class TimerTypeConverter : GsonTypeConverter() {
    @TypeConverter
    fun fromTimerToString(timer: List<Int>) = gson.toJson(timer)

    @TypeConverter
    fun fromStringToTimer(timer: String): List<Int> {
        if (timer != null && timer.isNotEmpty()) {
            val itemType = object : TypeToken<List<Int>>() {}.type
            return gson.fromJson<List<Int>>(timer, itemType)
        }
        return emptyList<Int>()


    }
}

