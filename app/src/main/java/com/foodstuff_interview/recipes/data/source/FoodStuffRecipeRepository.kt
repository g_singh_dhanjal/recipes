package com.foodstuff_interview.recipes.data.source

import com.foodstuff_interview.recipes.data.Result
import com.foodstuff_interview.recipes.data.model.Recipe
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class FoodStuffRecipeRepository(
    val recipeLocalDataSource: RecipesDataSource,
    val recipesRemoteDataSource: RecipesDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : RecipesRepository {

    override suspend fun getAllRecipes(): Result<List<Recipe>> {
        return recipeLocalDataSource.getAllRecipes()
    }

    override suspend fun getRecipeById(id: Int): Result<Recipe> {
        return  recipeLocalDataSource.getRecipeById(id)
    }

    override suspend fun insertAll(recipes: List<Recipe>) {
        recipeLocalDataSource.insertAll(recipes)
    }

}