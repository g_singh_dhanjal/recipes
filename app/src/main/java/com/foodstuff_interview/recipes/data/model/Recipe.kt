package com.foodstuff_interview.recipes.data.model

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.foodstuff_interview.recipes.data.database.typeconverters.IngredientsTypeConverter
import com.foodstuff_interview.recipes.data.database.typeconverters.StepsTypeConverter
import com.foodstuff_interview.recipes.data.database.typeconverters.TimerTypeConverter
import java.io.Serializable

@Entity
@Keep data class Recipe(
    @PrimaryKey(autoGenerate = true)
    val recipeId: Long ,
    val name: String,
    @TypeConverters(IngredientsTypeConverter::class)
    val ingredients: List<Ingredients>,
    @TypeConverters(StepsTypeConverter::class)
    val steps: List<String>,
    @TypeConverters(TimerTypeConverter::class)
    val timers: List<Int>,
    val imageURL: String,
    val originalURL: String?
):Serializable
