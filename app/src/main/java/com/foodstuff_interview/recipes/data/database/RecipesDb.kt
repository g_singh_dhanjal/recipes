package com.foodstuff_interview.recipes.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.foodstuff_interview.recipes.R
import com.foodstuff_interview.recipes.data.database.typeconverters.IngredientsTypeConverter
import com.foodstuff_interview.recipes.data.database.typeconverters.StepsTypeConverter
import com.foodstuff_interview.recipes.data.database.typeconverters.TimerTypeConverter
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.util.FileUtils
import com.foodstuff_interview.recipes.util.ParsingUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Recipe::class), version = 1)
@TypeConverters(
    IngredientsTypeConverter::class,
    StepsTypeConverter::class,
    TimerTypeConverter::class,
)
abstract class RecipesDb : RoomDatabase() {
    abstract fun recipesDao(): RecipesDao

    companion object {
        @Volatile
        private var instance: RecipesDb? = null
        private val LOCK = Any()


        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also{
                instance = it
            }
        }

        private fun buildDatabase(context: Context): RecipesDb {

            val recipeDb = Room.databaseBuilder(context, RecipesDb::class.java, "recipes")
                .addCallback(RecipesDatabaseCallback(CoroutineScope(SupervisorJob()),context = context))
                .build()
            instance = recipeDb
            return  recipeDb
        }

    }
    class RecipesDatabaseCallback(
        private val scope: CoroutineScope,
        private val context: Context,


    ) : Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            instance?.let { database ->
                scope.launch {
                    populateRecipeTable(context,database.recipesDao() )
                }
            }

            super.onCreate(db)
        }

        suspend fun populateRecipeTable (context: Context,recipesDao: RecipesDao) {
            // Delete all content here.
            recipesDao.deleteAll()
            val recipes = ParsingUtil().parseRecipeJson(FileUtils.readRecipeJsonFromAsset(context,context.getString(
                R.string.recipes_file_name)))
            for (recipeItem in recipes) {
                recipesDao.insert(recipeItem)
                println("Saving recipe to db ${recipeItem.toString()}")
            }

        }

    }
}