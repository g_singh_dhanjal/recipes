package com.foodstuff_interview.recipes.data.database.typeconverters

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import java.util.*

class StepsTypeConverter: GsonTypeConverter() {

    @TypeConverter
    fun fromStepsToString(steps: List<String>) = gson.toJson(steps)

    @TypeConverter
    fun fromStringToSteps(steps: String): List<String> {
        if (steps != null && steps.isNotEmpty()) {
            val itemType = object : TypeToken<List<String>>() {}.type
            return gson.fromJson<List<String>>(steps, itemType)
        }
        return emptyList<String>()


    }
}