package com.foodstuff_interview.recipes.data.source.local

import com.foodstuff_interview.recipes.data.Result
import com.foodstuff_interview.recipes.data.database.RecipesDao
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.data.source.RecipesDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RecipesLocalDataSource(
    val recipesDao: RecipesDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : RecipesDataSource {
    override suspend fun getAllRecipes(): Result<List<Recipe>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(recipesDao.getAllRecipe())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun getRecipeById(id: Int): Result<Recipe> = withContext(ioDispatcher) {

        return@withContext try {
            Result.Success(recipesDao.getRecipeById(id))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun insertAll(recipes: List<Recipe>) = withContext(ioDispatcher) {
        recipesDao.insertAll(recipes)
    }


}