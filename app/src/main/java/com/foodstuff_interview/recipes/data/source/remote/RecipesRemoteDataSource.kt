package com.foodstuff_interview.recipes.data.source.remote

import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.data.source.RecipesDataSource
import  com.foodstuff_interview.recipes.data.Result;
import java.lang.Exception

// TODO for future purposes , can be used to load recipes from a server instead of getting from local json file
class RecipesRemoteDataSource : RecipesDataSource {
    override suspend fun getAllRecipes(): Result<List<Recipe>> {
       return  Result.Error( Exception("Not Implemented"))
    }

    override suspend fun getRecipeById(id: Int): Result<Recipe> {
        return  Result.Error( Exception("Not Implemented"))
    }

    override suspend fun insertAll(recipes: List<Recipe>) {

    }
}