package com.foodstuff_interview.recipes.data.model

data class Ingredients(val quantity: String, val name: String, val type: String)
