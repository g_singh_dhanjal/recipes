package com.foodstuff_interview.recipes.data.database.typeconverters

import androidx.room.TypeConverter
import com.foodstuff_interview.recipes.data.model.Ingredients
import com.foodstuff_interview.recipes.data.model.Recipe
import com.google.gson.reflect.TypeToken

public class IngredientsTypeConverter : GsonTypeConverter() {

    @TypeConverter
    fun fromIngredientsToString(ingredients: List<Ingredients>) = gson.toJson(ingredients)

    @TypeConverter
    fun fromStringToIngredients(ingredients: String): List<Ingredients> {
       if (ingredients != null && ingredients.isNotEmpty()) {
            val itemType = object : TypeToken<List<Ingredients>>() {}.type
            return gson.fromJson<List<Ingredients>>(ingredients, itemType)
        }
        return emptyList<Ingredients>()


    }
}