package com.foodstuff_interview.recipes.data.database.typeconverters

import com.google.gson.Gson

open class GsonTypeConverter constructor() {
    public var gson: Gson = Gson()
}

