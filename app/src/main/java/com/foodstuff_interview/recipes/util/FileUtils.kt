package com.foodstuff_interview.recipes.util

import android.content.Context
import java.io.BufferedReader
import java.io.FileNotFoundException

class FileUtils {

     companion object {
         fun readRecipeJsonFromAsset(context: Context, fileName: String): String {
             try {
                return context
                     .assets
                     .open(fileName)
                     .bufferedReader()
                     .use(BufferedReader::readText)
             } catch (e: FileNotFoundException){

                 return ""
             }
         }
     }
}