package com.foodstuff_interview.recipes.util

import com.foodstuff_interview.recipes.data.model.Recipe
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import java.io.EOFException

class ParsingUtil {


    fun parseRecipeJson(recipeJson: String): List<Recipe> {
        try {
            if (recipeJson != null && recipeJson.isNotEmpty()) {
                var gson = Gson()
                val itemType = object : TypeToken<List<Recipe>>() {}.type
                return gson.fromJson<List<Recipe>>(recipeJson, itemType)
            }
        } catch (e: JsonSyntaxException) {
            e.printStackTrace()
        } catch (e: EOFException) {
            e.printStackTrace()
        }catch (e: JsonParseException) {
            e.printStackTrace()
        }

        return emptyList()

    }


}