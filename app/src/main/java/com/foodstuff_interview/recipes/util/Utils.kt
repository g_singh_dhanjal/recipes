package com.foodstuff_interview.recipes.util

import com.foodstuff_interview.recipes.data.model.Ingredients

class Utils {

    companion object {

        fun convertStepsListToString(steps: List<String>): String {
            var stepsString = ""
            if (steps.isNotEmpty() ) {

                for (step in steps) {
                    stepsString += step + "\n\n"
                }
            }

            return stepsString
        }



        fun convertIngredientsListToString(ingredients: List<Ingredients>): String {
            var ingredientString = ""
            if (ingredients.isNotEmpty() ) {

                for (ingredient in ingredients) {
                    ingredientString += ingredient.quantity +" "+ ingredient.name + "\n"
                }
            }

            return ingredientString
        }

        fun convertTimeListToTotalTime(times: List<Int>): Int {
            var totalTime = 0
            if (times.isNotEmpty() ) {

                for (time in times) {
                    totalTime += time
                }
            }
          println("Total time $totalTime")
            return totalTime

        }

    }
}