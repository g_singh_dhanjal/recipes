package com.foodstuff_interview.recipes.data.database

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.foodstuff_interview.recipes.data.model.Recipe
import com.foodstuff_interview.recipes.util.FileUtils
import com.foodstuff_interview.recipes.util.ParsingUtil
import kotlinx.coroutines.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RecipesDbTest {
    private lateinit var recipesDao: RecipesDao
    private lateinit var db: RecipesDb
    private lateinit var recipeModel: MutableList<Recipe>
    val scope = CoroutineScope(SupervisorJob())

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val recipeJson = FileUtils.readRecipeJsonFromAsset(context, "Recipes.json")
        val parsingUtil = ParsingUtil()
        recipeModel = parsingUtil.parseRecipeJson(recipeJson).toMutableList()

        db = Room.inMemoryDatabaseBuilder(
            context, RecipesDb::class.java
        ).addCallback(
            (object : RoomDatabase.Callback() {
                override fun onCreate(sdb: SupportSQLiteDatabase) {

                    scope.launch {
                        db.recipesDao().insertAll(recipeModel)
                    }
                    super.onCreate(sdb)
                }


            })
        ).build()
        recipesDao = db.recipesDao()

    }


    @Test
    fun test_getAllRecipes_ReturnsRecipeList() = runBlocking {

        val recipeList = recipesDao.getAllRecipe()
        assertThat(recipeList, notNullValue())
        assertThat(recipeList, hasSize(9))
        recipesDao.deleteAll()
    }
    @Test
    fun testForDelete_ReturnsEmptyRecipeList()= runBlocking{
        recipesDao.insertAll(recipeModel)
        recipesDao.deleteAll()
        val recipeList = recipesDao.getAllRecipe()
        assertThat(recipeList, hasSize(0))
    }

/*    @Test
    fun testInsertAll_recipeListFromJsonToDb_returnList() = runBlocking {

        recipesDao.deleteAll()
        println("Recipe Model size ${recipeModel.size}")
        val recipeListTemp = recipesDao.getAllRecipe()
        println("Recipe ModelrecipeListTemp size ${recipeListTemp.size}")
        recipesDao.insertAll(recipeModel)
        val recipeList = recipesDao.getAllRecipe()
        assertThat(recipeList, notNullValue())
        assertThat(recipeList, hasSize(9))
        recipesDao.deleteAll()
    }*/
    @Test
    fun test_getRecipeFromDb_returnRecipe() = runBlocking {
        recipesDao.insert(recipeModel[0])
        val recipe = recipesDao.getRecipeById(1)
        assertThat(recipe, notNullValue())
        print(recipe.name)
        assertThat(recipe.name, equalToIgnoringCase(recipeModel[0].name))
        recipesDao.deleteAll()
    }
    @After
    fun tearDown() {
        db.close()
        recipeModel.clear()
    }
}