package com.foodstuff_interview.recipes

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.foodstuff_interview.recipes.data.source.FoodStuffRecipeRepository
import com.foodstuff_interview.recipes.data.source.RecipesRepository
import com.foodstuff_interview.recipes.ui.main.recipes.RecipesViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Test
import org.junit.runner.RunWith




@RunWith(
    AndroidJUnit4::class)

class RecipesViewModelTest {


    private lateinit var recipesViewModel: RecipesViewModel


    private lateinit var mockRecipeTestRepository: RecipesRepository

    val context = ApplicationProvider.getApplicationContext<Context>()
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setupViewModel() {

        mockRecipeTestRepository = ServiceLocator.provideTasksRepository(context)
        GlobalScope.launch {
            mockRecipeTestRepository.getAllRecipes()
        }

        recipesViewModel = RecipesViewModel(mockRecipeTestRepository)



    }

    @Test
    fun testGetAllRecipes(){
        recipesViewModel.getRecipes()
        val value = recipesViewModel.recipes.getOrAwaitValue()
        assertThat(
            value,
            CoreMatchers.not(CoreMatchers.nullValue())

        )
        assertThat(value, hasSize(9))
    }
}
