package com.foodstuff_interview.recipes.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FileUtilsTest {

    val context = ApplicationProvider.getApplicationContext<Context>()


    @Test
    fun testReadRecipeJsonFromAsset_wrongFileName_returnEmptyString() {
        val jsonData = FileUtils.readRecipeJsonFromAsset(context, "Recip.json")
        assertThat(jsonData, `is`(isEmptyString()))
    }
    @Test
    fun testReadRecipeJsonFromAsset_blankFileName_returnEmptyString() {
        val jsonData = FileUtils.readRecipeJsonFromAsset(context, "")
        assertThat(jsonData, `is`(isEmptyString()))
    }
    @Test
    fun testReadRecipeJsonFromAsset_ValidFileName_returnJsonString() {
        val jsonData = FileUtils.readRecipeJsonFromAsset(context, "Recipes.json")
        print(jsonData)
        assertThat(jsonData, `is`(notNullValue()))
    }

}