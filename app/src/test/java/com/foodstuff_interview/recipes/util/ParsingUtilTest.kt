package com.foodstuff_interview.recipes.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.foodstuff_interview.recipes.data.model.Recipe
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ParsingUtilTest {

    lateinit var parsingUtil: ParsingUtil
    val context = ApplicationProvider.getApplicationContext<Context>()
    lateinit var recipeJson : String
    @Before
    public fun setUp() {
        parsingUtil = ParsingUtil()
        recipeJson =  FileUtils.readRecipeJsonFromAsset(context,"Recipes.json")
    }


    @Test
    fun testParseRecipeJson_withEmptyString_returnNull() {
        val recipeModel = parsingUtil.parseRecipeJson("")
        assertThat(recipeModel, notNullValue())
        assertThat(recipeModel, empty())
    }
    @Test
    fun testParseRecipeJson_withValidJsonString_returnRecipes() {
        val recipeModel = parsingUtil.parseRecipeJson(recipeJson)
        assertThat(recipeModel, notNullValue())
        //assertThat(recipeModel, instanceOf(List<Recipe>::class.java))
        assertThat(recipeModel, iterableWithSize(9))
        assertThat(recipeModel, hasSize(9))
    }

    @Test
    fun testParseRecipeJson_withJsonStringSingleRecord_returnRecipe() {
        val recipeModel = parsingUtil.parseRecipeJson("[{\"name\":\"Crock Pot Roast\",\"ingredients\":[{\"quantity\":\"1\",\"name\":\" beef roast\",\"type\":\"Meat\"},{\"quantity\":\"1 package\",\"name\":\"brown gravy mix\",\"type\":\"Baking\"},{\"quantity\":\"1 package\",\"name\":\"dried Italian salad dressing mix\",\"type\":\"Condiments\"},{\"quantity\":\"1 package\",\"name\":\"dry ranch dressing mix\",\"type\":\"Condiments\"},{\"quantity\":\"1/2 cup\",\"name\":\"water\",\"type\":\"Drinks\"}],\"steps\":[\"Place beef roast in crock pot.\",\"Mix the dried mixes together in a bowl and sprinkle over the roast.\",\"Pour the water around the roast.\",\"Cook on low for 7-9 hours.\"],\"timers\":[0,0,0,420],\"imageURL\":\"http://img.sndimg.com/food/image/upload/w_266/v1/img/recipes/27/20/8/picVfzLZo.jpg\",\"originalURL\":\"http://www.food.com/recipe/to-die-for-crock-pot-roast-27208\"}]")
        assertThat(recipeModel, notNullValue())
        assertThat(recipeModel, hasSize(1))
    }
    @Test
    fun testParseRecipeJson_withFaultyJson_returnEmptyList() {
        val recipeModel = parsingUtil.parseRecipeJson("[{\"name\":\"Crock Pot Roast\",\"ingredients\":[{\"quantity\":\"1\",\"name\":\" beef roast\",\"type\":\"Meat\"},{\"quantity\":\"1 package\",\"name\":\"brown gravy mix\",\"type\":\"Baking\"},{\"quantity\":\"1 package\",\"name\":\"dried Italian salad dressing mix\",\"type\":\"Condiments\"},{\"quantity\":\"1 package\",\"name\":\"dry ranch dressing mix\",\"type\":\"Condiments\"},{\"quantity\":\"1/2 cup\",\"name\":\"water\",\"type\":\"Drinks\"}],\"steps\":[\"Place beef roast in crock pot.\",\"Mix the dried mixes together in a bowl and sprinkle over the roast.\",\"Pour the water around the roast.\",\"Cook on low for 7-9 hours.\"],\"timers\":[0,0,0,420],\"imageURL\":\"http://img.sndimg.com/food/image/upload/w_266/v1/img/recipes/27/20/8/picVfzLZo.jpg\",\"originalURL\":\"http://www.food.com/recipe/to-die-for-crock-pot-roast-27208\"}")
        assertThat(recipeModel, notNullValue())
        assertThat(recipeModel, empty())
    }
}