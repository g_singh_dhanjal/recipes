package com.foodstuff_interview.recipes.util

import com.foodstuff_interview.recipes.data.model.Ingredients
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test

class UtilsTest {


    @Before
    fun setUp() {

    }

    @Test
    fun convertStepsListToString_listOfSteps_returnValidString() {

        val stepsList = listOf<String>(
            "ARRANGE chicken in a single layer in a large pot.",
            "Add water to just cover.",
        )
        val string = Utils.convertStepsListToString(stepsList)
        assertThat(string, not(isEmptyOrNullString()))
    }
    @Test
    fun convertIngredientsListToString_EmptylistOfISteps_returnEmptyString() {
        val stepsList = emptyList<String>(
        )
        val string = Utils.convertStepsListToString(stepsList)
        assertThat(string, isEmptyString())
    }

    @Test
    fun convertIngredientsListToString_listOfIngredients_returnValidString() {
        val ingredientList = listOf<Ingredients>(
            Ingredients("3", "skinless, boneless chicken breasts, halved lengthwise", "Baking"),
            Ingredients("1/2 cup", "mayonnaise", "Baking"),
        )
        val string = Utils.convertIngredientsListToString(ingredientList)
        assertThat(string, not(isEmptyOrNullString()))
    }


    @Test
    fun convertIngredientsListToString_EmptylistOfIngredients_returnEmptyString() {
        val ingredientList = emptyList<Ingredients>(
        )
        val string = Utils.convertIngredientsListToString(ingredientList)
        assertThat(string, isEmptyString())
    }
    @Test
    fun convertTimeListToTotalTime_ListOfTime_returnTotalTime() {

        val listOfTime = listOf(0, 10, 10)
        val totalTime = Utils.convertTimeListToTotalTime(listOfTime)
        assertThat(totalTime, comparesEqualTo(20))
    }
    @Test
    fun convertTimeListToTotalTime_EmptyListOfTime_returnTotalTimeZero() {

        val listOfTime = emptyList<Int>()
        val totalTime = Utils.convertTimeListToTotalTime(listOfTime)
        assertThat(totalTime, comparesEqualTo(0))
    }
}